package december;

import java.util.Scanner;

public class SecondPart3 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter your first number: ");
		double number1 = Double.parseDouble(myScanner.nextLine());
		System.out.println("Enter the operator: ");
		System.out.println("('p' to print out both numbers, 'b' to find largest value, 's' to find smallest value)");
		char operator = myScanner.nextLine().charAt(0);
		System.out.println("Enter your second number: ");
		double number2 = myScanner.nextDouble();

		myScanner.close();
		double result;

		switch (operator) {
		case '+':
			result = number1 + number2;
			System.out.println(result);
			break;
		case '-':
			result = number1 - number2;
			System.out.println(result);
			break;
		case '/':
			result = number1 / number2;
			System.out.println(result);
			break;
		case '*':
			result = number1 * number2;
			System.out.println(result);
			break;
		case '^':
			result = Math.pow(number1, number2);
			System.out.println(result);
			break;
		case '%':
			result = number1 % number2;
			System.out.println(result);
			break;
		case 'p':
			System.out.println(number1 + ";" + number2);
			break;
		case 'b':
			if (number1 > number2)
				System.out.println(number1 + " is the largest");
			else
				System.out.println(number2 + " is the largest");

			break;
		case 's':
			if (number1 > number2)
				System.out.println(number2 + " is the smallest");
			else
				System.out.println(number1 + " is the smallest");
			break;
		default:
			System.out.println("Something went wrong!");
			break;
		}

	}

}
