package december;

import java.util.Scanner;

public class SecondPart1 {

	public static void main(String[] args) {
		int DayOfWeek;

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter day number: ");
		DayOfWeek = Integer.parseInt(myScanner.nextLine());
		myScanner.close();

		if (DayOfWeek > 7 || DayOfWeek < 1) {
			System.out.println("Please enter value that is in range 1-7.");
			return;
		}
		switch (DayOfWeek) {
		case 1, 2, 3, 4, 5:
			System.out.println("It is a working day!");
			break;
		case 6, 7:
			System.out.println("It is holiday!");
			break;

		}
	}

}
