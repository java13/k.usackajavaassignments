package december;

import java.util.Scanner;

public class SecondPart2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter your grade: ");
		char gradeLevel = myScanner.next().charAt(0);

		myScanner.close();

		switch (gradeLevel) {
		case 'A', 'B':
			System.out.println("Perfect! You are so clever!");
			break;
		case 'C':
			System.out.println("Good! But You can do better!");
			break;
		case 'D', 'E':
			System.out.println("It is not good! You should study!");
			break;
		case 'F':
			System.out.println("Fail! You need to repeat the exam!");
			break;
		default:
			System.out.println("Please, enter your real grade!");
			break;
		}

	}

}
