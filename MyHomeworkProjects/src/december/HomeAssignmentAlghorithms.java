package december;

public class HomeAssignmentAlghorithms {
	public static void main(String[] args) {
		double a;
		double b;
		a = Math.pow(2, 3);
		b = Math.pow(3, 2);

		if (a > b)
			System.out.println("2^3 > 3^2");
		else if (b > a)
			System.out.println("2^3 < 3^2");
		else
			System.out.println("2^3 = 3^2");
	}
}
