package december;

import java.util.*;

public class FirstTask {

	public static String spacecraftName;
	public static String spacecraftManufacturer;
	public static int totalLaunches;
	public static boolean spacecraftStatus;
	public static double costPerLaunch;
	public static int firstFlightYear;

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter name of Spacecraft!");
		FirstTask.spacecraftName = myScanner.nextLine();

		System.out.println("Enter Manufacturer of the Spacecraft!");
		FirstTask.spacecraftManufacturer = myScanner.nextLine();

		System.out.println("Total Launches!");
		FirstTask.totalLaunches = Integer.parseInt(myScanner.nextLine());

		System.out.println("Enter Cost per Launch (million US$)");
		FirstTask.costPerLaunch = Double.parseDouble(myScanner.nextLine());

		System.out.println("What Year the Spacecraft had it's first flight?");
		FirstTask.firstFlightYear = Integer.parseInt(myScanner.nextLine());
		myScanner.close();
		show();

	}

	public static void show() {
		System.out.println("Spacecraft: " + FirstTask.spacecraftName);
		System.out.println("Manufacturer: " + FirstTask.spacecraftManufacturer);
		System.out.println("Total Launches: " + FirstTask.totalLaunches);
		System.out.println("Cost per Launch: " + FirstTask.costPerLaunch + " US $");
		System.out.println("Year of first flight: " + FirstTask.firstFlightYear);

	}

}