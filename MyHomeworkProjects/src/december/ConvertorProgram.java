package december;

import java.util.Scanner;

public class ConvertorProgram {

	public static double moneyUSDEUR;
	public static double lenghtMF;
	public static double temperatureCF;
	public static double temperatureCK;
	public static double areaMKm;
	public static double volumeML;
	public static double massKgLbs;
	public static double timeHD;

	public static void main(String[] args) {

		System.out.println("Enter values you want to convert! (Results will be at the end)");
		System.out.println();
		System.out.println("1. Convert USD to EUR.");
		Scanner myScanner = new Scanner(System.in);
		ConvertorProgram.moneyUSDEUR = Double.parseDouble(myScanner.nextLine());

		System.out.println("2. Convert Meters to Feet.");
		ConvertorProgram.lenghtMF = Double.parseDouble(myScanner.nextLine());

		System.out.println("3. Convert Celsius to Fahrenheit");
		ConvertorProgram.temperatureCF = Double.parseDouble(myScanner.nextLine());

		System.out.println("4. Convert Celsius to Kelvin.");
		ConvertorProgram.temperatureCK = Double.parseDouble(myScanner.nextLine());

		System.out.println("5. Convert m� to km�.");
		ConvertorProgram.areaMKm = Double.parseDouble(myScanner.nextLine());

		System.out.println("6. Convert m� to l.");
		ConvertorProgram.volumeML = Double.parseDouble(myScanner.nextLine());

		System.out.println("7. Convert Kg to Pounds.");
		ConvertorProgram.massKgLbs = Double.parseDouble(myScanner.nextLine());

		System.out.println("8. Convert Hours to Days.");
		ConvertorProgram.timeHD = Double.parseDouble(myScanner.nextLine());
		System.out.println();
		show();
		myScanner.close();
	}

	public static void show() {

		System.out.println("Results!");
		double convertedMoneyEUR = (moneyUSDEUR * 0.82);
		String calculationsEUR = "1. " + moneyUSDEUR + " USD = " + convertedMoneyEUR + " EUR";
		System.out.println(calculationsEUR);

		double convertedLenghtFt = (lenghtMF * 3.28084);
		String calculationsFt = "2. " + lenghtMF + " m = " + convertedLenghtFt + " Ft";
		System.out.println(calculationsFt);

		double convertedTemperatureF = (temperatureCF * 1.8) + 32;
		String calculationsF = "3. " + temperatureCF + " �C = " + convertedTemperatureF + " �F";
		System.out.println(calculationsF);

		double convertedTemperatureK = temperatureCK + 273.15;
		String calculationsK = "4. " + temperatureCK + " �C = " + convertedTemperatureK + " K";
		System.out.println(calculationsK);

		double convertedAreaKm = areaMKm * 0.000001;
		String calculationsKm = "5. " + areaMKm + " m� = " + convertedAreaKm + " km�";
		System.out.println(calculationsKm);

		double convertedVolumeL = volumeML * 1000;
		String calculationsL = "6. " + volumeML + " m� = " + convertedVolumeL + " l";
		System.out.println(calculationsL);

		double convertedMassLbs = massKgLbs * 2.2046226218;
		String calculationsLbs = "7. " + massKgLbs + " kg = " + convertedMassLbs + " lbs";
		System.out.println(calculationsLbs);

		double convertedTimeHD = timeHD / 24;
		String calculationsDays = "8. " + timeHD + " hours = " + convertedTimeHD + " days";
		System.out.println(calculationsDays);
		toTheOtherSide();

	}

	public static double moneyEURUSD;
	public static double lenghtFM;
	public static double temperatureFC;
	public static double temperatureKC;
	public static double areaKmM;
	public static double volumeLM;
	public static double massLbsKg;
	public static double timeDH;

	public static void toTheOtherSide() {
		System.out.println();
		System.out.println();

		System.out.println("Now convert to the other side! (Results will be at the end)");
		System.out.println();
		System.out.println("1. Convert EUR to USD.");
		Scanner myScanner = new Scanner(System.in);
		ConvertorProgram.moneyEURUSD = Double.parseDouble(myScanner.nextLine());

		System.out.println("2. Convert Feet to Meters.");
		ConvertorProgram.lenghtFM = Double.parseDouble(myScanner.nextLine());

		System.out.println("3. Convert Fahrenheit to Celsius.");
		ConvertorProgram.temperatureFC = Double.parseDouble(myScanner.nextLine());

		System.out.println("4. Convert Kelvin to Celsius.");
		ConvertorProgram.temperatureKC = Double.parseDouble(myScanner.nextLine());

		System.out.println("5. Convert km� to m�.");
		ConvertorProgram.areaKmM = Double.parseDouble(myScanner.nextLine());

		System.out.println("6. Convert l to m�.");
		ConvertorProgram.volumeLM = Double.parseDouble(myScanner.nextLine());

		System.out.println("7. Convert Pounds to Kg.");
		ConvertorProgram.massLbsKg = Double.parseDouble(myScanner.nextLine());

		System.out.println("8. Convert Days to Hours.");
		ConvertorProgram.timeDH = Double.parseDouble(myScanner.nextLine());
		System.out.println();
		System.out.println();
		myScanner.close();
		show1();
	}

	public static void show1() {

		System.out.println("Results!");
		double convertedMoneyUSD = moneyEURUSD * 1.21;
		String calculationsUSD = "1. " + moneyEURUSD + " EUR = " + convertedMoneyUSD + " USD";
		System.out.println(calculationsUSD);

		double convertedLenghtM = lenghtFM * 0.3048;
		String calculationsM = "2. " + lenghtFM + " Ft = " + convertedLenghtM + " m";
		System.out.println(calculationsM);

		double convertedTemperatureFC = ((temperatureFC - 32) * 0.55555555556);
		String calculationsFC = "3. " + temperatureFC + " �F = " + convertedTemperatureFC + " �C";
		System.out.println(calculationsFC);

		double convertedTemperatureKC = temperatureKC - 273.15;
		String calculationsKC = "4. " + temperatureKC + " K = " + convertedTemperatureKC + " �C";
		System.out.println(calculationsKC);

		double convertedAreaM = areaKmM * 1000000;
		String calculationsKmM = "5. " + areaKmM + " km� = " + convertedAreaM + " m�";
		System.out.println(calculationsKmM);

		double convertedVolumeM = volumeLM * 0.001;
		String calculationsLM = "6. " + volumeLM + " l = " + convertedVolumeM + " m�";
		System.out.println(calculationsLM);

		double convertedMassKg = massLbsKg / 2.205;
		String calculationsKg = "7. " + massLbsKg + " lbs = " + convertedMassKg + " kg";
		System.out.println(calculationsKg);

		double convertedTimeDH = timeDH * 24;
		String calculationsHours = "8. " + timeDH + " days = " + convertedTimeDH + " hours";
		System.out.println(calculationsHours);

	}
}
