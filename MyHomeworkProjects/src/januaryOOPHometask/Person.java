package januaryOOPHometask;

public class Person {

	protected String name, surname;
	
	public Person() {}	// non-argument constructor
	
	public Person(String name, String surname) {	// argument constructor
		this.name = name;
		this.surname = surname;
	}
	
	public String getName() {	// getters and setters
		return name;
	}
	
	public void setName(String name) {	// getters and setters
		this.name = name;
	}
	
	public String getSurname() {	// getters and setters
		return surname;
	}
	
	public void setSurname(String surname) {	// getters and setters
		this.surname = surname;
	}
	
	public String toString() {
		return "Name: " + this.name + "\nSurname: " + this.surname;
	}
	
}
