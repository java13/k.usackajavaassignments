package januaryOOPHometask;

import java.util.ArrayList;

public class District {

	private String title, city;
	private int districtID;
	private ArrayList<Officer> officers = new ArrayList<Officer>(); 

	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {

		return "The title: " + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are: " + this.officers.size()
				+ " officers in the district";
	}

	public void addNew(Officer officer) {
		this.officers.add(officer);
	}

	public void removeOfficer(Officer officer) {
		this.officers.remove(officer); // remove the corresponding officer from the list
	}
	
	public ArrayList<Officer> getOfficers() {
		return officers;
	}
	
	public void setOfficers(ArrayList<Officer> officers) {
		this.officers = officers;
	}

	public float calculateLevel() {
		float levelSum = 0;

		for (int i = 0; i < this.officers.size(); i++) {
			levelSum += this.officers.get(i).calculateLevel();
		}
		return levelSum / this.officers.size();

	}

}
