package januaryOOPHometask;

import java.util.ArrayList;
import java.util.Iterator;

public class Execution {

	public static void main(String[] args) {
		
// creating 7 officers as objects
		Officer[] officers = new Officer[7];	
		
		for(int i = 0; i < officers.length; i++) {	
			officers[i] = new Officer();
		}
		
// creating 2 districts as objects
		District district1 = new District("District 4", "New York", 8786334);	
		District district2 = new District("District 89", "Chicago", 1611845);	
		
// creating 3 lawyers
		Lawyer lawyer1 = new Lawyer("Gene", "Snyder", 1249, 22);
		Lawyer lawyer2 = new Lawyer("Luther", "Wilkerson", 3629, 48);
		Lawyer lawyer3 = new Lawyer("Orville", "Burns", 8846, 37);
		
// adding 3 officers to first district
		for(int i = 0; i < 3; i++) {		
			district1.addNew(officers[i]);
		}
// adding 4 officers to second district
		for(int i = 3; i < officers.length; i++) {	
			district2.addNew(officers[i]);
		}
		
// printing all info
		System.out.println("DISTRICTS:");
		System.out.println(district1.toString());	
		System.out.println("");	
		System.out.println(district2.toString());	
		System.out.println("--------------------------");// to get more readable result in console
		System.out.println("LAWYERS:");
		System.out.println(lawyer1.toString());		
		System.out.println("");
		System.out.println(lawyer2.toString());		
		System.out.println("");
		System.out.println(lawyer3.toString());		
		System.out.println("--------------------------");
		
// creating ArrayList for storing lawyers
		ArrayList<Lawyer> lawyers = new ArrayList<Lawyer>();	
		lawyers.add(lawyer1);		// adding lawyers to the ArrayList
		lawyers.add(lawyer2);
		lawyers.add(lawyer3);
		
// declaring variable to count total crimes involving lawyers
		int totalLawyerCrimes = 0;	
// for loop to count total crimes involving lawyers
		for(int i = 0; i < lawyers.size(); i++) {	
			totalLawyerCrimes += lawyers.get(i).getHelpedInCrimesSolving();
		}
		
		System.out.println("All solved crimes with lawyers involved: " + totalLawyerCrimes);	// printing the result
		
// finding best lawyer-helper:
		Lawyer best = null;
		
		Iterator<Lawyer> iterator = lawyers.iterator();	// declaring an iterator to go trough all values
		while(iterator.hasNext()) {
			Lawyer current = iterator.next();	// declaring current value so I could compare best value with current value while going trough all values
			if(best == null || best.getHelpedInCrimesSolving() < current.getHelpedInCrimesSolving()) {	// comparing
				best = current;	// if current value is greater than the best then assigning current to best
			}
		}
		
		
		System.out.println("--------------------------");	// printing result
		System.out.println("Biggest lawyer-helper when solving crimes:\n" + best);
		
		
		
		
		
		
	}
}
