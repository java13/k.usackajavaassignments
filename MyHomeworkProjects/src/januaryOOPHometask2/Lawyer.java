package januaryOOPHometask2;

public class Lawyer extends Person {

	private int lawyerID, helpedInCrimesSolving;
	
	public Lawyer() {}	// non-argument constructor
	
	public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving){	// argument constructor
		super(name, surname);
		this.lawyerID = lawyerID;
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}
	
	public int getLawyerID() {	// getters and setters
		return lawyerID;
	}
	
	public void setLawyerID(int lawyerID) {	// getters and setters
		this.lawyerID = lawyerID;
	}
	
	public int getHelpedInCrimesSolving() {	// getters and setters
		return helpedInCrimesSolving;
	}
	
	public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {	// getters and setters
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}
	
	public String toString() {
		return super.toString() + 
				 "\nID: " + this.lawyerID + "\nHelped to solve crimes: " + 
				this.helpedInCrimesSolving;
	}
	
	
}
