package januaryOOPHometask2;

import java.util.ArrayList;

public class Execution {

	public static void main(String[] args) {
		
// adding 7 officer objects
		
		Officer[] officers = new Officer[7];
		
		for(int i = 0; i < officers.length; i++) {
			officers[i] = new Officer();
		}
		
// adding 2 districts		
		District district1 = new District("District 1", "Los Angeles", 3588034);
		District district2 = new District("District 2", "Huston", 8767334);
		
		
// adding 3 lawyers
		Lawyer[] lawyers = new Lawyer[3];
		
		for(int i = 0; i < lawyers.length; i++) {
			lawyers[i] = new Lawyer();
		}
		
// adding 3 officers and  2 lawyers to district 1
		for(int i = 0; i < 3; i++) {
			district1.addNewPerson(officers[i]);
		}
		for(int i = 0; i < 2; i++) {
			district1.addNewPerson(lawyers[i]);
		}
// adding rest of the officers and lawyers to district 2
		for(int i = 3; i < officers.length; i++) {
			district2.addNewPerson(officers[i]);
		}
		for(int i = 2; i < lawyers.length; i++) {
			district2.addNewPerson(lawyers[i]);
		}
	
// printing all info about both districts		
		System.out.println(district1.toString());
		System.out.println();
		System.out.println(district2.toString());
		
// storage for both districts
		ArrayList<District> storage = new ArrayList<District>();
		storage.add(district1);
		storage.add(district2);
		
		System.out.println();
		

// finding the district with highest amount of people		
		if(storage.get(0).getPerson().size() > storage.get(1).getPerson().size()) {
			System.out.println("District 1 is with highest amount of persons.");
		}	
		else if(storage.get(0).getPerson().size() < storage.get(1).getPerson().size()){
			System.out.println("District 2 is with highest amount of persons.");
		}	
		else {
			System.out.println("Both districts have the same amount of persons.");
		}
		
		
	}

}
