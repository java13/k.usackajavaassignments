package januaryOOPHometask2;

import java.util.ArrayList;

public class District {

	private String title, city;
	private int districtID;
	private ArrayList<Person> personsInTheDistrict = new ArrayList<Person>(); 

	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {

		return "The title: " + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are: " + this.personsInTheDistrict.size()
				+ " persons in the district";
	}

	public void addNewPerson(Person person) {
		this.personsInTheDistrict.add(person);
	}

	public void setPerson(ArrayList<Person> person) {
		this.personsInTheDistrict = person;

	}

	public ArrayList<Person> getPerson() {
		return personsInTheDistrict;
	}

	public void removePerson(Person person) {
		this.personsInTheDistrict.remove(person); // remove the corresponding officer from the list
	}
	
	
	
// calculating average level in a district
	public float calculateAverageLevel() {
		
		float levelSum = 0;
		float countOfficers = 0;

		for (int i = 0; i < personsInTheDistrict.size(); i++) {	
			
			if(this.personsInTheDistrict.get(i) instanceof Officer) {	// checking if the person from a district is an officer
				
				levelSum += ((Officer) this.personsInTheDistrict.get(i)).calculateLevel();	// counting all officer levels together
				countOfficers++;	// counting officers
			}
		}
		return levelSum / countOfficers;	// calculating average level in a district

	}

}
