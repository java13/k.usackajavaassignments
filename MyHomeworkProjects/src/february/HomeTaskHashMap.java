package february;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class HomeTaskHashMap {

	static int productsUnderEuro = 0;
	static double lowestProductPrice = 0, highestProductPrice = 0;
	static Map<Double, String> sortedByPrices;

	public static void main(String[] args) {

		Map<String, Double> productsAndPrices = new HashMap<String, Double>();
		productsAndPrices.put("Milk", 0.59);
		productsAndPrices.put("White bread", 0.32);
		productsAndPrices.put("Buckwheat", 2.29);
		productsAndPrices.put("Magazine", 3.28);
		productsAndPrices.put("Orange juice", 1.27);
		productsAndPrices.put("Carrots", 0.27);
		productsAndPrices.put("Sour cream", 1.09);
		productsAndPrices.put("Flour", 1.19);

		findCheapestProduct(productsAndPrices);
		findMostExpensive(productsAndPrices);
		findProductsUnderEur(productsAndPrices);
		printAllFoundProductCount(productsUnderEuro, lowestProductPrice, highestProductPrice);

		Map<String, Double> moreProductsAndPrices = new HashMap<String, Double>();
		moreProductsAndPrices.put("Cheese", 3.49);
		moreProductsAndPrices.put("Coconut", 1.49);
		moreProductsAndPrices.put("Water", 0.45);
		moreProductsAndPrices.put("Chocolate", 2.28);
		moreProductsAndPrices.put("French fries", 2.19);
		moreProductsAndPrices.put("Ice cream", 0.79);
		moreProductsAndPrices.put("Lolipop", 0.15);

		productsAndPrices.putAll(moreProductsAndPrices);
		System.out.println("There are total of " + productsAndPrices.size() + " products in this basket.");

		System.out.println();
		sortProducts(productsAndPrices);
		printSortedProducts(sortedByPrices);

	}

	private static void findProductsUnderEur(Map<String, Double> productsAndPrices) {

		Iterator<Double> iterator = productsAndPrices.values().iterator();
		while (iterator.hasNext()) {
			double currentProductPrice = iterator.next();
			if (currentProductPrice < 1) {
				productsUnderEuro++;
			}
		}

	}

	private static void findCheapestProduct(Map<String, Double> productsAndPrices) {

		Iterator<Double> iterator = productsAndPrices.values().iterator();
		while (iterator.hasNext()) {
			double currentProductPrice = iterator.next();
			if (lowestProductPrice == 0 || currentProductPrice < lowestProductPrice) {
				lowestProductPrice = currentProductPrice;
			}
		}
	}

	private static void findMostExpensive(Map<String, Double> productsAndPrices) {

		Iterator<Double> iterator = productsAndPrices.values().iterator();
		while (iterator.hasNext()) {
			double currentProductPrice = iterator.next();
			if (highestProductPrice == 0 || currentProductPrice > highestProductPrice) {
				highestProductPrice = currentProductPrice;
			}
		}
	}

	private static void sortProducts(Map<String, Double> productsAndPrices) {
		// the treemap sorts all values automatically so as i need that the map is
		// sorted by prices, i will create treemap
		// and add product name as value and product price as key

		// storing all product names in String[]
		String[] productName = new String[productsAndPrices.size()];

		Iterator<String> nameIterator = productsAndPrices.keySet().iterator();

		while (nameIterator.hasNext()) {
			for (int i = 0; i < productsAndPrices.size(); i++) {
				productName[i] = nameIterator.next();
			}
		}

		// storing all product prices in double[]
		double[] productPrice = new double[productsAndPrices.size()];

		Iterator<Double> priceIterator = productsAndPrices.values().iterator();

		while (priceIterator.hasNext()) {
			for (int j = 0; j < productsAndPrices.size(); j++) {
				productPrice[j] = priceIterator.next();
			}

		}

		// creating a treemap where key is product price and value is product name
		sortedByPrices = new TreeMap<Double, String>();
		for (int i = 0; i < productsAndPrices.size(); i++) {
			sortedByPrices.put(productPrice[i], productName[i]);
		}
	}

	private static void printAllFoundProductCount(int productsUnderEuro, double lowestProductPrice,
			double highestProductPrice) {

		if (productsUnderEuro == 1) {
			System.out.println("There are " + productsUnderEuro + " product that cost under 1 EUR.");
		}

		else {
			System.out.println("There are " + productsUnderEuro + " products that cost under 1 EUR.");
		}

		System.out.println("Lowest price in this product basket is " + lowestProductPrice + " EUR.");
		System.out.println("Highest price in this product basket is " + highestProductPrice + " EUR");
	}

	private static void printSortedProducts(Map<Double, String> sortedProducts) {

		System.out.println("Products sorted in ascending order by prices:");
		Iterator<Double> iterator = sortedProducts.keySet().iterator();
		while (iterator.hasNext()) {
			for (int i = 1; i < sortedProducts.size() + 1; i++) {
				double currentProductPrice = iterator.next();
				System.out.println(i + ")Price: " + currentProductPrice + " EUR; Product: " 
								+ sortedProducts.get(currentProductPrice)); // here important to remember that in 
																		  //sortedProducts map product name is value 
																		  //and price - key
			}

		}
	}
}
