package february;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class HomeTaskHashTable {
	// declaring all variables
	static int productsUnderEur;
	static double lowestProductPrice, highestProductPrice;
	static Map<String, Double> sortedByPrices ;

	public static void main(String[] args) {

		Hashtable<String, Double> products = new Hashtable<String, Double>();
// adding 8 products
		products.put("milk", 0.69);
		products.put("carrots", 1.11);
		products.put("bread", 0.88);
		products.put("rice", 0.96);
		products.put("orange juice", 1.87);
		products.put("fozen pizza", 3.48);
		products.put("bananas", 8.78);
		products.put("winter gloves", 3.46);
// calling methods
		countProductsUnderEuro(products);
		printProductsUnderEuro(productsUnderEur);
		findLowestPrice(products);
		printLowestPrice(lowestProductPrice);
		findHighestPrice(products);
		printHighestPrice(highestProductPrice);
// adding more products
		Hashtable<String, Double> moreProducts = new Hashtable<String, Double>();
		moreProducts.put("eggs", 1.13);
		moreProducts.put("cabbage", 1.46);
		moreProducts.put("magazine", 3.28);
		moreProducts.put("jogurt", 0.69);
		moreProducts.put("tea", 0.29);
		moreProducts.put("frozen peas", 3.44);
		moreProducts.put("chocolate", 1.56);
		moreProducts.put("pickles", 2.14);
		moreProducts.put("canned fish", 0.89);
		moreProducts.put("gift card", 50.0);
// merging both product Hashtables
		products.putAll(moreProducts);
// calling methods
		printTotalProductCount(products);
		sortProducts(products);
		System.out.print(System.lineSeparator());
		printSortedProducts(sortedByPrices);
		

	}

	private static void countProductsUnderEuro(Hashtable<String, Double> products) {

		Iterator<Double> priceIterator = products.values().iterator();
		while (priceIterator.hasNext()) {
			double currentPrice = priceIterator.next();
			if (currentPrice < 1) {
				productsUnderEur++;
			}
		}
	}

	private static void findLowestPrice(Hashtable<String, Double> products) {

		Iterator<Double> priceIterator = products.values().iterator();

		while (priceIterator.hasNext()) {
			double currentProductPrice = priceIterator.next();
			if (lowestProductPrice == 0 || lowestProductPrice > currentProductPrice) {
				lowestProductPrice = currentProductPrice;
			}
		}
	}

	private static void findHighestPrice(Hashtable<String, Double> products) {

		Iterator<Double> priceIterator = products.values().iterator();

		while (priceIterator.hasNext()) {
			double currentProductPrice = priceIterator.next();
			if (highestProductPrice == 0 || highestProductPrice < currentProductPrice) {
				highestProductPrice = currentProductPrice;
			}
		}
	}

	private static void sortProducts(Hashtable<String, Double> products) {
// placing all products in a list
		List<Map.Entry<String, Double>> productSort = new ArrayList<Map.Entry<String, Double>>(products.entrySet());
// using collections class 'sort' method sorting all values in ascending order by values
		Collections.sort(productSort, new Comparator<Map.Entry<String, Double>>() {
		// comparing all price values	
			public int compare(Entry<String, Double> price1, Entry<String, Double> price2) {
				return price1.getValue().compareTo(price2.getValue());
			}
			
		});
		// storing all products in sorted order in linkedhashmap
		sortedByPrices = new LinkedHashMap<String, Double>();
		
		for (Entry<String, Double> product : productSort) {
			sortedByPrices.put(product.getKey(), product.getValue());
		}

	}

	private static void printProductsUnderEuro(int productCount) {

		if (productCount == 1) {
			System.out.println("There are " + productCount + " product that cost less than 1 euro in the basket.");
		} else {
			System.out.println("There are " + productCount + " products that cost less than 1 euro in the basket.");
		}
	}

	private static void printLowestPrice(double productPrice) {

		System.out.println("Lowest product price in this basket is " + productPrice + " EUR.");
	}

	private static void printHighestPrice(double productPrice) {
		
		System.out.println("Highest product price in this basket is " + productPrice + " EUR.");
	}

	private static void printTotalProductCount(Hashtable<String, Double> products) {
		
		System.out.println("There are " + products.size() + " products in this basket.");
	}
	
	private static void printSortedProducts(Map <String, Double>sortedProducts) {
		
// iterating and printing all products in ascending order by price
		System.out.println("Products sorted in ascending order by prices:");
		Iterator<String> iterator = sortedProducts.keySet().iterator();
		while(iterator.hasNext()) {
			for(int i = 1; i < sortedProducts.size() + 1; i++) {
				String currentProductKey = iterator.next();
				System.out.println(i + ")Price: " +  sortedProducts.get(currentProductKey)
						+ " EUR; Product: " + currentProductKey);
			}
			
		}
	}
}
